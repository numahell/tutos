---
title: Changer la couleur du texte d'un PDF avec Gimp
summary: Votre imprimante n'a plus d'encre noire ? Voici comment changer la couleur de texte d'un PDF avec Gimp
status: draft
---

# Changer la couleur du texte d'un PDF avec Gimp

Ouvrez le PDF dans Gimp. Une fois ouvert, un seul calque est disponible. L'idée sera de superposer un nouveau calque de couleur, et de choisir un mode de fusion pour que le texte ressorte avec cette couleur. Cela fonctionne bien si le fond est blanc.

1. Choisir la couleur de premier plan : selon ce qu'il vous reste de couleur, un mélange de cyan et de magenta peut permettre d'avoir assez de contraste.

    ![Choisir la couleur](./images/capture-gimp-calques-couleur-addition.png)

2. Ajouter un calque par dessus le premier, avec les paramètres suivants :

    - Mode : addition
    - Remplir avec : couleur de premier plan

    ![Ajouter un calque](./images/capture-gimp-ajouter-calque.png)

    Vous devriez avoir deux calques visibles :

    - le calque constitué par le PDF, en mode Normal
    - le calque complètement rempli de la couleur, en mode Addition

    ![Liste de calques](./images/capture-gimp-calques-couleur-addition.png)

3. Fusionner les calques visibles (clic droit sur un calque)

4. Exporter au format .pdf, sous un autre nom pour ne pas perdre le document d'origine

Et voilà :

![Résultat pdf](./images/capture-evince-pdf-bleu.png)
