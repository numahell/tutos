# Tutoriels

Quelques tutoriels fait pour des ateliers divers, sous la licence [CC-By-SA](http://creativecommons.org/licenses/by-sa/4.0/).

Attention, je ne suis que développeuse, certains tutoriels ont été fait dans une pratique non professionnelle.

## Graphisme

- [Tuto Inkscape](inkscape/flyer-inkscape.md)
- [Changer la couleur d'un texte avec Gimp](gimp/gimp-couleur-pdf.md)

## Cartographie

- [Tuto Umap](umap/umap.md)

[![Licence Creative
Commons](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)\
Ces tutoriels sont mis à disposition selon les termes de la [Licence
Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0
International](http://creativecommons.org/licenses/by-sa/4.0/).
