---
title: Créer un flyer avec Inkscape
summary: Créer un flyer avec Inkscape
separator: <!--s-->
verticalSeparator: <!--v-->
slideOptions:
  transition: slide
  theme: solarized
---

# Créer un flyer avec Inkscape

Notes:

*Ce tuto a été créé pour un atelier du Pic le 30 novembre 2019*

<!--s-->

## C'est quoi Inkscape ?

<!--v-->

* Logiciel libre de bureau
* Dessin vectoriel
* Pour GNU/Linux, Windows ou MacOS

![Logo Inkscape](https://media.inkscape.org/static/images/inkscape-logo.svg)

<!--v-->

### Dessin vectoriel ?!

Deux types d'images

<!--v-->

#### Image Bitmap

(ou raster)

* Matrice de pixels
* Information sur la couleur de chaque pixel
* Formats : `.png`, `.jpg`, `.tiff`

![](./images/pixels-from-garonne-gregory-tonon-CC-By-SA.png)

<!--v-->

#### Image Vectorielle

* Description en SVG (schéma XML)
* Formes avec contour et couleur, dégradés, objets textes, groupes d'objets, …
* Formats : `.svg`, `.svgz`

<!--v-->

### Documentation

Pas mal de contenu inspiré du
[Manuel libre sur Inkscape](https://flossmanualsfr.net/initiation-inkscape/loutil-de-selection/), édité par [Floss Manuals](https://www.flossmanualsfr.net/) sous licence GPLv2

<!--v-->

### Créer un document

* Ouvrir Inkscape

Format par défaut : A4

<!--v-->

### Interface

![Interface](./images/interface-inkscape092-haut.png)

<!--v-->

#### Interface (2)

![Interface](./images/interface-inkscape092-bas.png)

<!--s-->

## Premiers objets vectoriels

<!--v-->

### Premier objet

* Créer un rectangle ![rectangle](./images/outil-rectangle.png)
* Outil sélection ![sélection](./images/outil-selection.png)
  - Déplacer
  - Transformer

<!--v-->

### Remplissage et contour

* changer la couleur
* modifier le contour

![remplissage](./images/inkscape-remplissage.png) ![contours](./images/inkscape-contours.png)

<!--v-->

### Créer des formes géométriques

![Formes géométriques](/images/formes-geometriques.png)

<!--v-->

#### Outils

* Rectangle ![rectangle](./images/outil-rectangle.png)
* Cercle, ellipse, arc de cercle ![ellipse](./images/outil-ellipse.png)
* Étoile et polygones ![etoile](./images/outil-etoile.png)
* Spirale ![spirale](./images/outil-spirale.png)

<!--v-->

### Les unités

* `mm`, `cm`, `in` : pour l'impression
* `pt`, impression, surtout taille de polices
* `px`, affichage écrans

<!--v-->

### Le texte

* écrire un texte
  - directement
  - dans un cadre
* modifier taille
* modifier le style

<!--v-->

### Créer des formes libres

![Forme libre](./images/formes-libres.png)

<!--v-->

#### Outils

* Crayon ![Crayon](./images/outil-crayon.png)
* Courbes de Béziers ![Courbe](./images/outil-bezier.png)
* Tracé calligraphique ![Calligraphie](./images/outil-crayon.png)

<!--v-->

### Modifier les formes

* Édition des nœuds
* Sculpter les formes
* Effacer des chemins
* Outil remplissage

<!--s-->

## Concevoir un flyer

<!--v-->

### Questions à se poser

* Quel message ?
* Pour qui ?
* Objectif du flyer ?
* Durée de vie ?

<!--v-->

### Format

Les plus courants

* A5
* Carte postale → A6
* Tryptique → A4 plié en 3

<!--v-->

### Contenu

* Logo, illustration, couleurs de l'asso
* Flyer pérenne : vos activités
* Pour un événement : date et lieu
* Contact, site web

<!--v-->

### Conseils pour la forme

* Couleurs : 3 maxi
* Typographies : 2 polices maxi
  * Éviter le souligné, peu lisible
* Message : clair et concis
* Images : résolution de 300dpi
* Disposition : aérer le contenu

<!--v-->

### Faites tester le flyer !

Si possible par quelqu'un qui ne connait pas votre association.

<!--v-->

### Des exemples

<!--v-->

![](./exemples/contribatelier.png)

<!--v-->

![](./exemples/flyer-pic-recto.png)

<!--v-->

![](./exemples/toulibre-recto.png)

<!--s-->

## Créer mon flyer

<!--v-->

### Commencer

- Propriétés du document
  - Dimensions A5
  - Afficher des grilles

→ Aide pour magnétiser les objets

<!--v-->

#### Espace de dessin

- Dessiner un carré → Objets en guides

→ Définit des marges

<!--v-->

#### Fonds

Deux possibilités

- Propriétés du document → couleur de fond
- Dessiner un carré de couleur

<!--v-->

#### Utiliser les calques

- Un calque pour le fond
- Un autre pour le texte
- Un troisième pour les éléments modifiables dans le temps (optionnel)

Évite de déplacer des éléments d'autres calques

<!--v-->

### Textes

- écrire les textes : titre de l'événement, date, lieu
- modifier taille, couleur des textes
- écrire texte plus long, encadré pour le résumé
- modifier le style

<!--v-->

### Organiser les éléments

- aligner les éléments
- dupliquer les éléments

<!--v-->

### Créer des formes complexes

Exemple d'un nuage : fusion de plusieurs cercles de couleur

- fusionner, exclure
- modifier les nœuds

<!--v-->

### texte autour d'une forme


<!--s-->

## Merci !

Lisez le [manuel d'initiation à Inkscape](https://flossmanualsfr.net/initiation-inkscape/)  
chez Floss Manual

<!--v-->

### Crédits

Tutoriel sous licence [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)

Sauf:

- image pixellisée issue de ["Toulouse Plages"](http://www.flickr.com/photos/eriatarka31/7624636092) par *Gregory Tonon*, **CC-By-SA**
- pictos outils et interface issue de [Initiation Inkscape](https://flossmanualsfr.net/initiation-inkscape/) en GPLv2
- exemples de flyers Contribatelier par Marnic (et image de David Revoy), le PIC au Pic, et Toulibre à Toulibre
