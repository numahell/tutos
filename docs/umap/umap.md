---
title: Créer une carte avec Umap
summary: Créer une carte avec Umap
---

# Créer une carte avec Umap

*À l'origine [publié le 3 juillet 2019 sur le blog de Makina Corpus](https://makina-corpus.com/blog/metier/2019/creer-une-carte-avec-umap), ce tutoriel s'adresse aux personnes souhaitant créer des cartes interactives avec l'outil Umap*.

[Umap](https://wiki.openstreetmap.org/wiki/UMap) est une application en ligne libre qui permet de créer et d'éditer ses propres cartes, en utilisant les données géographiques libres de **OpenStreetMap**. Elle est basée sur [Django](https://edit.makina-corpus.com/realisations?subject=Django) et [Leaflet](https://edit.makina-corpus.com/realisations?subject=Webmapping), deux technologies libres que nous utilisons beaucoup chez Makina Corpus. Et bien sûr, le code source [est disponible ici](https://github.com/umap-project/umap).

L'objectif de ce tutoriel est de créer une carte comme celle présentée ci-dessous, intégrant des données récoltées sur le [portail OpenData de Toulouse Métropole](https://data.toulouse-metropole.fr/pages/accueil/) par exemple. Ce tutoriel est souvent proposé chez Makina Corpus aux stagiaires en observation pour acquérir une première approche de la cartographie web.

[![](./images/umap-00-carte.png)](http://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845#13/43.6067/1.4477)

*Carte des bibliothèques, espaces de coworking et autres tiers-lieux toulousains*

## Pour commencer

### Créer une carte

Sur [umap.openstreetmap.fr](http://umap.openstreetmap.fr/), créez un compte. L'avantage de créer un compte est de pouvoir retrouver et lister ses cartes, et de restreindre la modification par d'autres utilisateurs du site.

Il est également conseillé de créer un compte sur OpenStreetMap afin d'y contribuer par la suite.

### Les propriétés

Donnez un nom pour la carte et l'enregistrer. ![](./images/umap-01-nom-carte.png)

![](./images/umap-02-proprietes-carte.png)

*Propriétés de la carte*

Il est possible de modifier l'icône par défaut et en choisir une autre plus esthétique.

![](./images/umap-03-proprietes-icones.png)

*Mise en forme des icones*

## Définir les limites géographiques

Cherchez Toulouse dans le champs de recherche, éventuellement ajuster le zoom puis définissez les limites géographiques et le zoom par défaut de la carte en cliquant sur le bouton ![Save this center and zoom](images/umap-01-save-center-zoom.png).

Coordonnées GPS utilisées 

- latitude = 43,607835
- longitude = 1,435604

## Choisir un fond de carte

Umap met à disposition plusieurs fonds de carte, c'est à dire des styles de cartes, ou comment les informations géographiques (routes, bâtiments, cours d'eau…) sont représentées. Le choix d'un fond de carte dépend souvent des informations à mettre en avant.

![](./images/umap-04-fond-carte.png)

*Choisir un fond de carte en fonction de la nature des données*

Par exemple, si vos données sont liées à la pratique du vélo, préférez le fond de carte "OSMOpenCycleMap" qui met en avant les pistes cyclables. Si vous souhaitez mettre les cours d'eau en valeur, le fond "OSMHydra" est fait pour vous. J'ai d'ailleurs choisi ce dernier, plutôt pour son esthétique épurée.

## Ajouter des objets

Il est temps maintenant d'ajouter des éléments à notre carte.

Les éléments géographiques que l'on peut ajouter à la carte sont de trois ordres : point, ligne, polygone.

![](./images/umap-05-ajout-edit-formes.png)

*Les formes possibles dans Umap*

## Importer des données

Les formats de données sont nombreux. Il faut distinguer les données contenant des informations géographiques de celles qui n'en contiennent pas.

- données : csv, json, xml, …
- données géographiques  : GeoJSON (le plus courant), kml, gpx, shapefile, …

![Par exemple le geojson](./images/umap-06-data-format-json.png)

*Par exemple le geojson*

### Ce qu'il faut savoir sur les licences

Pour avoir l'autorisation de réutiliser des données, il faut que la licence le permette. Pour l'OpenData, il existe deux licences [reconnues par l'État français pour l'OpenData](https://www.data.gouv.fr/fr/licences) : 

- [ODbL](https://opendatacommons.org/licenses/odbl/) (Open Database Licence)
- [Licence Ouverte](https://github.com/etalab/licence-ouverte/blob/master/LO.md)

### Récupérer des données

Le portail d'OpenData de Toulouse Métropole propose un grand nombre de jeux de données géographiques réutilisables. Par exemple, nous pouvons récupérer les [données des bibliothèques](https://data.toulouse-metropole.fr/explore/dataset/mediatheques-bibliotheques-et-bibliobus/information/) au format GeoJSON, qui fournissent les coordonnées géographiques mais également les noms des bibliothèques.

### Importer les données dans Umap

Une fois les données téléchargées, cliquez sur la flèche, et importez le fichier. Choisissez le format de données et le calque où les importer (ici, un nouveau calque).

![](./images/umap-06-import-data.png)

*Importer des données dans la carte*

Il est également possible de donner directement une URL, ou bien de coller les données dans un champs texte.

## Autres fonctionnalités

### Gestion des calques

Nous l'avons vu, les données ont été importées dans un nouveau calque. Les calques regroupent un ensemble d'objets géographiques, ils permettent ensuite à l'utilisateur d'afficher ou de cacher des données et de lister les données ainsi regroupées. Un calque correspond souvent à un jeu de données.

Le style des points, lignes ou polygones peut être défini au niveau de chaque calque. Ainsi lorsque nous importons de nouvelles données, il sera facile de les différencier en changeant simplement la couleur des pictogrammes.

![](./images/umap-07-liste-calques.png)

*Un calque par jeu de données*

### Interactions

Lorsque l'on clique sur un élément, une popup s'affiche avec des informations supplémentaires. Il est possible de configurer quelles sont propriétés qui s'affichent dans cette popup. Dans notre cas, c'est le champs `nom` des propriétés de chaque objet que l'on souhaite afficher dans la popup, avec éventuellement le type de bibliothèque.

![](./images/umap-08-popup-affichage.png)

Dans le calque où sont les données des bibliothèques, ajoutez dans "Gestion du contenu de la popup" les noms des propriétés à afficher. Ce champs utilise le [markdown](https://daringfireball.net/projects/markdown/), en mettant "#" devant le nom, celui-ci s'affiche sous forme de titre.

![](./images/umap-08-popup-data.png)

*Une popup paramétrable*

### Visualisation

Il est possible de définir une visualisation différente, par exemple de regrouper les <abbr title="Point
of Interest">POI</abbr> en utilisant l'affichage en clusters.

![Exemple affichage de cluster](./images/umap-08-affichage-cluster.png)

[Vous pouvez visualiser la carte ici](http://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845#13/43.6066/1.4483)

*Regrouper les points en "clusters"*


#### Densité

Pour représenter des données de densité, épaisseur ou couleur pondérée, utilisez la visualisation heatmap. Ça peut être [des lignes plus ou moins épaisses selon le trafic](https://makina-corpus.com/realisations/analyse-et-construction-de-routes-maritimes), ou bien [un changement de couleur selon la hauteur des vagues](https://makina-corpus.com/realisations/amelioration-du-metocean-analytics).

![](https://makina-corpus.com/realisations/amelioration-du-metocean-analytics/openoceanvtzoom.gif)

*Représentation de la hauteur des vagues en heatmap (mais pas Umap)*

### Droits d'édition

Par défaut la carte est publique et disponible dans la liste des cartes. Il est possible de restreindre la visualisation à des personnes ayant un compte sur Umap, mais également de leur partager l'édition de la carte.

![](./images/umap-08-gestion-droits-modif.png)

*Gestion des droits d'accès et d'édition*

## Ressources

### Umap

- wiki OpenStreetMap [wiki.openstreetmap.org/wiki/UMap](https://wiki.openstreetmap.org/wiki/UMap)
- Tutoriels en français [wiki.cartocite.fr](https://wiki.cartocite.fr/doku.php?id=umap:tutoriel_umap)

### OpenStreetMap, la cartographie libre

OpenStreetMap est une base de données géographiques libres, fruit de nombreux contributeurs et contributrices.

Elle propose également l'affichage de cartes, le calcul d'itinéraire, mais également la contribution aux données géographiques.

[openstreetmap.org](http://openstreetmap.org/)

### OpenData, données ouvertes

Les données ouvertes sont des données que chacun a le droit de réutiliser sans restriction du moment que la source est indiquée et la licence utilisée identique. Celles-ci sont anonymisées et ne sont en aucun cas des données personnelles.

Les licences permettant la réutilisation des données sont en général "ODbL" ou "Licence Ouverte", et le format doit être utilisable pour des traitements automatisés. Nous l'avons dit, ces deux licences sont [reconnues par l'État français pour l'OpenData](https://www.data.gouv.fr/fr/licences).

Dans les deux cas, il est nécessaire de mentionner la source (l'organisme qui a fourni ces données). Pour réutiliser des données sous licence ouverte, il est demandé d'indiquer en plus la date de dernière mise à jour des données. La différence notable est l'obligation pour les bases de données dérivées de données sous licence ODbL, d'être sous la même licence (ou compatible).

À noter que pour les fonds de carte, qui sont assimilés à une création graphique, les licences utilisées sont plutôt les [Creative commons](http://creativecommons.fr/). 

Des portails de données ont été mis à disposition par l'État et des collectivités territoriales comme par exemple [data.gouv.fr](https://www.data.gouv.fr/fr/) ou [data.toulouse-metropole.fr](https://data.toulouse-metropole.fr/pages/accueil/).
